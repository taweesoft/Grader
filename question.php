<?php
	session_start();
	require_once('connection.php');
	$topicID = $_GET['id'];
	$sql = "SELECT * FROM tbl_data WHERE topicID='$topicID' ORDER BY id";
	$result = mysql_query($sql);
	$infomation = "";
?>
<html>
<head>
	<link rel="stylesheet" href="css/fm.scrollator.jquery.css" />
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/question_style.css" />
</head>

<body>	
		<div class='wrapper'>
			<div id="scroller">
			<?php
				$data = array();
				$first = true;
				$firstID = 0;
				while($row = mysql_fetch_array($result)){
					$id= $row['id'];
					$head = $row['head'];
					$info = $row['info'];
					$passed = $row['passed'];
					array_push($data, $id,$head,$passed);
					echo "<div id='card".$id."' class='card' onclick='selected(".$i.")' >";
					echo "<p id='topic'>" .$head ."</p>";
					// echo "<p hidden id='info'>" .$info ."</p>";
					echo "<p id='passed'>" .$passed ." passed</p>";
					echo "</div>";
					if($first){
						$firstID = $id;
						$information = $info;
						$first = false;
					}
				}
			?>
		</div>
		<div id='full'>
			<div id='topicHeader'>
				<p id='topicText'> </p>
			</div>

			<p id='questionText'>QUESTION</p>
			<div id='information'>
				<?php
					echo $information;
				?>
			</div>
			<div class='IO'>
				<?php
					$sql_IO = "SELECT * FROM tbl_IO WHERE questionID='$firstID'";
					$result_IO = mysql_query($sql_IO);
					if(mysql_num_rows($result_IO)>0){
						echo "<div class='perIO'>";
						echo "<pre class='text1'>Input</pre>";
						echo "<pre class='text1'>Output</pre>";
						echo "</div>";
					}
					while($row_IO = mysql_fetch_array($result_IO)){
						$input = $row_IO['input'];
						$output = $row_IO['output'];
						echo "<div class='perIO'>";
						echo "<pre class='IOText'>".$input."</pre>";
						echo "<pre class='IOText'>".$output."</pre>";
						echo "</div>";
					}
				?>
			</div>
			<div id='codeBlock'>
					<div id="editor">public static void main(String[] args){

}</div>
			</div>
		</div>
		</div>
		
	<script src="js/jquery.js"></script>
	<script src="js/fm.scrollator.jquery.js"></script>
	<script src="ace-highlighter/src-min/ace.js" type="text/javascript" charset="utf-8"></script>
	<script>
		$(function () {
			var $scrollable_div1 = $('#scroller');
			$scrollable_div1.scrollator();
			var $scrollable_div2 = $('#full');
			$scrollable_div2.scrollator();

			var topicText = $('#topicText');
			var firstTopic = $('#topic');
			topicText.text(firstTopic.text());

			var informationText = $('#informationText');
			var firstInfo = $('#info');
			informationText.text(firstInfo.text());
			var editor = ace.edit("editor");
   			editor.setTheme("ace/theme/xcode");
   			editor.getSession().setMode("ace/mode/java");
		});
	</script>
	
</body>

</html>