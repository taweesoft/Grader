<?php
	require_once('auth.php');
	require_once('connection.php');

	$sql = "SELECT * FROM tbl_menu ORDER by id";
	$result= mysql_query($sql);
	
?>

<html>
<head>
	<link href="css/menu_style.css" rel="stylesheet" />
	<link rel="Stylesheet" type="text/css" href="css/smoothDivScroll.css" />
</head>

<body>
	<div id="bigImg">
		<p>GRADER</p>
	</div>
	<div id="makeMeScrollable">
		<?php
		$i = 0;
		while($row = mysql_fetch_array($result)){
			$i++;
			echo "<div id='card".$i."' class='card' onclick='selected(".$i.")' >";
			echo "<p id='topic'>" .$row['topic'] ."</p>";
			echo "<p id='info'>" .$row['info'] ."</p>";
			echo "</div>";
		}
		?>
		<!-- <img src="images/demo/field.jpg" alt="Demo image" id="field" />
		<img src="images/demo/gnome.jpg" alt="Demo image" id="gnome" />
		<img src="images/demo/pencils.jpg" alt="Demo image" id="pencils" />
		<img src="images/demo/golf.jpg" alt="Demo image" id="golf" />
		<img src="images/demo/river.jpg" alt="Demo image" id="river" />
		<img src="images/demo/train.jpg" alt="Demo image" id="train" />
		<img src="images/demo/leaf.jpg" alt="Demo image" id="leaf" />
		<img src="images/demo/dog.jpg" alt="Demo image" id="dog" /> -->
	</div>

	<!-- LOAD JAVASCRIPT LATE - JUST BEFORE THE BODY TAG 
	     That way the browser will have loaded the images
		 and will know the width of the images. No need to
		 specify the width in the CSS or inline. -->

	<!-- jQuery library - Please load it from Google API's -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>

	<!-- jQuery UI (Custom Download containing only Widget and Effects Core)
	     You can make your own at: http://jqueryui.com/download -->
	<script src="js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
	
	<!-- Latest version (3.1.4) of jQuery Mouse Wheel by Brandon Aaron
	     You will find it here: https://github.com/brandonaaron/jquery-mousewheel -->
	<script src="js/jquery.mousewheel.min.js" type="text/javascript"></script>

	<!-- jQuery Kinectic (1.8.2) used for touch scrolling -->
	<!-- https://github.com/davetayls/jquery.kinetic/ -->
	<script src="js/jquery.kinetic.min.js" type="text/javascript"></script>

	<!-- Smooth Div Scroll 1.3 minified-->
	<script src="js/jquery.smoothdivscroll-1.3-min.js" type="text/javascript"></script>

	<!-- If you want to look at the uncompressed version you find it at
	     js/jquery.smoothDivScroll-1.3.js -->

	<!-- Plugin initialization -->
	<script type="text/javascript">
		// Initialize the plugin with no custom options
		$(document).ready(function () {
			// None of the options are set
			$("div#makeMeScrollable").smoothDivScroll({
				autoScrollingMode: "onStart"
			});
        
		});
		function selected(index){
			window.location='question.php?id='+index;
        };
		
	</script>
</body>
	
</html>