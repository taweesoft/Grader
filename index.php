<?php
  session_start();
  unset($_SESSION['username']);
?>

<!DOCTYPE html>
<html >
  <head>
    <title>SOS camp 4th Grader</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel='stylesheet prefetch' href='css/animate.min.css'>
    <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="css/style.css">
  </head>

  <body>
    <div class='form animated bounceIn'>
      <div class='switch'>
        <i class='fa fa-pencil fa-times'></i>
      </div>
      <div class='login'>
        <h2>Login</h2>
        <!-- left -->
        <div class="login-left">
          <h1>Welcome to<br><br>SOS Camp 4<span class="sup">th</span></h1>
        </div>
        <!-- /left -->
        <div class="divider"></div>
        <!-- right -->
        <div class="login-right">
          <form  method="POST" action="login.php">
            <input id='username' name="username" placeholder='Username' type='text'>
            <input id='password' name="password" placeholder='Password' type='password'>
            <button id='login' name="login">Login</button>
            <h1 id='login-message' class="login-message"><?php
              if(isset($_SESSION['err_msg'])){
                echo $_SESSION['err_msg'];
                unset($_SESSION['err_msg']);
              }?></h1>
          </form>
        </div>
        <!-- /right -->
      </div>
      <div class='register'>
        <h2>Create An Account</h2>
        <form method="POST" action='register.php'>
          <input id='username' name='username' placeholder='Username' type='text'>
          <input id='password' name='password' placeholder='Password' type='password'>
          <input id='repassword' name='repassword' placeholder='Confirm Password' type='password'>
          <input name='email' placeholder='Email Address' type='email'>
          <button id="register" name='register'>Create</button>
          <h1 id='login-message' class="login-message"><?php
            if(isset($_SESSION['err_msg'])){
              echo $_SESSION['err_msg'];
              unset($_SESSION['err_msg']);
            }?></h1>
        </form>
      </div>
    </div>
      <script src='js/jquery.js'></script>
      <script src="js/index.js"></script>
      <script type="text/javascript">
      // $('#login-message').hide();
        function check_empty(){
          if($('#username').val() == '' || $('#password').val() == ''){
            // $('#login-message').show();
            return false;
          }else{
            return true;
          }
        }
      </script>
  </body>
</html>
